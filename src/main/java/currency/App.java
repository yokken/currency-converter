package currency;

import java.util.Scanner;

public class App 
{
    public static void main( String[] args )
    {
        Scanner scanner = new Scanner(System.in);
        Database db = Database.getInstance();

        while (true) {

            System.out.println("\nMenu Options\n");
            System.out.println("(1) - Wybierz walute");
            System.out.println("(2) - Wyjscie");

            System.out.print(":");
            int selection = scanner.nextInt();

            if (selection == 1) {
                System.out.print("Dostepe waluty");
                int counter = 0;
                for(Currency c : db.getCurrencyList())
                {
                    if((counter++) % 5 == 0)
                        System.out.println();
                    System.out.print(c.getCode() + "\t\t\t");
                }
                System.out.println();


                System.out.print("Wybierz kod waluty z ktorej chcesz przeliczyc:");
                String src = scanner.next();
                System.out.print("Wybierz kod waluty na ktora chcesz zamienic:");
                String dest = scanner.next();
                System.out.print("Podaj kwote:");
                Double amount = scanner.nextDouble();

                Double res = ExChange.ExChangeCurrency(db.getCurrencyByCode(src), db.getCurrencyByCode(dest), amount);

                System.out.println(amount + " " + src + " = " + res + " " + dest);
            } else if (selection == 2) {
                break;
            }
        }



    }
}
