package currency;

import java.util.ArrayList;

public class Database {
    private ArrayList<Currency> currencyList;
    private Downloader _downloader;
    private DataConverter _dataConverter;
    private static Database instance;

    private Database()
    {
        _downloader = new Downloader();
        _dataConverter = new DataConverter();

        String rawData = _downloader.getRawData("https://www.nbp.pl/kursy/xml/LastA.xml");
        this.currencyList = _dataConverter.Convert(rawData);
    }

    public static Database getInstance() {
        if(instance == null) {
            instance = new Database();
        }
        return instance;
    }

    public Database(ArrayList<Currency> listawalut)
    {
        this.currencyList = listawalut;
    }

    public ArrayList<Currency> getCurrencyList()
    {
        return currencyList;
    }

    public Currency getCurrencyByCode(String code)
    {
        for(Currency x : this.currencyList) {
            if (x.getCode().equals(code)) {
                return x;
            }
        }
        throw new NullPointerException("No currency for given code: " + code);
    }
}
