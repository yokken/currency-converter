package currency;

import java.math.BigDecimal;

public class Currency {
    private String Name;
    private String Code;
    private Double Mid;

    public Currency() {

    }

    public Currency(String name, String code, Double mid) {
        Name = name;
        Code = code;
        Mid = mid;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public Double getMid() {
        return Mid;
    }

    public void setMid(Double mid) {
        Mid = mid;
    }

}
