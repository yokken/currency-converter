package currency;

import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class DataConverter {

    public ArrayList<Currency> Convert(String rawData){
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        ArrayList<Currency> result = new ArrayList<Currency>();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new ByteArrayInputStream(rawData.getBytes("ISO-8859-2"))));
            NodeList walutaList = doc.getElementsByTagName("pozycja");
            for(int i=0; i<walutaList.getLength();i++){
                Node p =  walutaList.item(i);
                if(p.getNodeType()==Node.ELEMENT_NODE){
                    Element waluta = (Element) p;
                    String pozycja= waluta.getAttribute("pozycja");
                    NodeList List= waluta.getChildNodes();

                    ArrayList<Element> elements = new ArrayList<Element>();
                    for(int j=0;j<List.getLength();j++){
                        Node n=List.item(j);
                        if(n.getNodeType()==Node.ELEMENT_NODE){
                            Element atrybut = (Element) n;
                            elements.add(atrybut);

                        }
                    }

                    result.add(new Currency(
                            elements.get(0).getTextContent(),
                            elements.get(2).getTextContent(),
                            Double.parseDouble(elements.get(3).getTextContent().replaceAll(",", "."))
                    ));

                }
            }
        } catch (SAXException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        result.add(new Currency("Polski zloty", "PLN", 1.));
        return result;
    }
}
