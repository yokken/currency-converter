package currency;

import java.io.*;
import java.net.URL;
import java.util.StringTokenizer;

public class Downloader {
    public Downloader() {
    }

    public String getRawData(String url) {
        StringBuilder sb = new StringBuilder();
        try {
            URL urll = new URL(url); //TODO name

            BufferedReader reader = new BufferedReader(new InputStreamReader(urll.openStream(), "ISO-8859-2"));
            String t;
            while((t = reader.readLine()) != null) {
                sb.append(t);
            }
            reader.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return sb.toString();
    }

    public static void getFile(String fname, String urlString){
        try {
            URL url = new URL(urlString);
            BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
            StringTokenizer st = new StringTokenizer(url.getFile(),"/");
            System.out.println("URL: "+url);
            while (st.hasMoreTokens())
            {
                fname = st.nextToken();
            }
            BufferedWriter writer = new BufferedWriter(new FileWriter(fname));
            String temp;
            while ((temp = reader.readLine()) != null)
            {
                writer.write(temp + "\n");
            }
            reader.close();
            writer.close();
        }catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }
}
